PROPOSITION DE STATUTS POUR ESPRIT FAB-LAB

6 décembre 2016 – Agnès Ravel

### PRÉAMBULE

#### Apprendre 

Nous favorisons l’acquisition, la transmission et la production des
savoirs et des savoir-faire par l’expérimentation, le faire soi-même,
l’échange de pairs à pairs et la pratique collective. Nous valorisons
les savoirs et les pratiques plus que les diplômes et l’expérience. Nous
laissons la place à des méthodes alternatives d’apprentissage, dont
l’essai-erreur, la co-formation par les pairs, les échanges de
savoirs... au rythme de chacun. Notre objectif est de permettre à chacun
d’acquérir plus d’autonomie, grâce au collectif et à l’entraide.

#### Fabriquer

Nous sommes un atelier de fabrication convivial, ouvert au plus grand
nombre . Il permet de prototyper, créer ou produire des objets "justes"
au sens de Yvan Illich, c’est à dire qu’"il est générateur d’efficience
sans dégrader l’autonomie personnelle, il ne suscite ni esclave ni
maître, et élargit le rayon d’action personnel" (extrait de La
Convivialité) .. Nous proposons un lieu où il est donné aux participants
la liberté de façonner les objets qui les entourent et le droit
d’utiliser leur énergie de façon créative, dans le respect des personnes
et de l’environnement. Nous recherchons l’appropriation et la maîtrise
par chacun des objets et des techniques, qui permet de valoriser
l’“intelligence de la main” et la créativité, et la réunion du penser et
du faire.

#### Partager

Nous nous engageons à favoriser le partage d’expériences, de savoirs, de
procédés, de codes, de plans, d’outils, etc. au sein de notre
association, dans le respect du droit de la propriété intellectuelle.
Cette valeur de partage se manifeste notamment par une promotion et une
incitation au recours aux licences libres et/ou ouvertes, qui selon nous
favorisent la circulation des savoirs. Nous souhaitons que toutes les
créations (matérielles ou immatérielles), produites au sein d’Esprit
Fablab ou suite aux échanges dans le cadre de l’association, profitent à
l’ensemble de l’humanité, qu’elles puissent être diffusées et améliorées
librement, tout en valorisant et reconnaissant les personnes à l’origine
de celle-ci.

Nous souhaitons plus généralement favoriser l’entraide et valoriser les
apports et connaissances de chacun.

#### Rencontrer

Nous souhaitons mettre en place un lieu permettant la rencontre, la
création de réseau, la diffusion d’information, l’échange de services,
etc. Nous pensons que cette création de réseaux informels est une aide
précieuse dans le domaine professionnel des personnes et qu’elle permet
l’émergence de nouveaux projets, qui peuvent également se développer
en-dehors de l’association.\
Nous envisageons notre association au sein de plusieurs autres entités
juridiques, existantes ou à naître, dont les activités sont connexes à
celles d’Esprit FabLab.

#### Et contribuer à changer le monde

Nous sommes un espace d’échange entre des personnes très différentes
(âge, sexe, culture, niveau de connaissance, activités, etc.) ; cette
diversité est une richesse. Nous expérimentons et accompagnons de
nouveaux modes de coopération, de financement, de travail, de diffusion,
d’éducation, de conception et de production.

Nous souhaitons édifier une société où "l’exercice de la créativité
d’une personne n’impose jamais à autrui un travail, un savoir ou une
consommation obligatoire" (Illch, La Convivialité). Nous souhaitons
accompagner la débrouille, la bidouille, l’envie d’entreprendre,
l’enpowerment, l’innovation écologique par et pour les habitants du
Centre Bretagne.

Nous souhaitons ainsi participer à un changement sociétal et espérons
avoir un impact positif. Ce souhait est assumé, recherché et encouragé.

### Article 1 - Constitution

Il est constitué, entre les adhérents aux présents statuts, une
association régie par la loi du premier juillet 1901 et le décret du 16
août 1901 ayant pour nom : ESPRIT FABLAB.

### Article 2 – Objet

L'association Esprit Fab-Lab a pour but d’être un lieu de rencontre
permettant le partage de savoirs, le développement de la culture et la
création d’outils (au sens large) afin que chacun puisse changer la vie
et façonner l’image de son propre avenir.

L'association veut permettre et faciliter la découverte, l'innovation,
et le partage de connaissances par la pratique, et au travers de la
collaboration de chacun, dans un esprit de transversalité et de respect
de l'environnement

### Article 3 - Siège social

Le siège social est délibérément fixé à : ???

Le siège social pourra être transféré par simple décision du Conseil
d'Administration.

### Article 4 – Durée

La durée de l'association est illimitée. Cependant, la raison d'être de
l’association est de permettre aux adhérent-es de mener en mode
collaboratif des actions cohérentes avec l'objet défini en article 2 des
statuts. Si l’association devait dans le futur en arriver au point de
devoir sacrifier ce mode "action " qui caractérise la culture commune de
ses adhérents dans le seul but de perpétuer sa propre existence, ce
serait en contradiction avec ses propres statuts et une négation de sa
raison d'être, l’association n'aurait alors comme issue que sa
dissolution.

### Article 5 : Règlement Intérieur

Un Règlement Intérieur validé en Assemblée Générale régit le
fonctionnement de l'association au delà des aspects spécifiés par les
présents statuts.

Il n'est pas figé et fait l'objet d'un processus d' élaboration
participative permanent, selon les modalités définies dans son
préambule.

### Article 6 – Composition de l’association

Sont membres de l’association Esprit FabLab toute personne physique et
morale ???, à jour de cotisation, adhérant aux présents statuts et aux
valeurs définies dans le préambule. Le montant des cotisations est
indiqué dans le règlement intérieur. Les membres s’engagent à respecter
les statuts et le règlement intérieur.

L’association est composée de :

\* membres actifs : ils participent régulièrement aux activités de
l’association, sont à jour de leur cotisation annuelle et leur adhésion
est validée par l’Assemblée Générale. Ils disposent d’un droit de vote à
l’Assemblée Générale et sont éligibles au Conseil d’Administration.

\* membres usagers : ils participent aux activités proposées par
l’association. Ils disposent d’une voix consultative et sont invités à
l’Assemblée Générale.

\* membres bienfaiteurs : ce sont toutes les personnes proposées par le
Conseil d’Administration et cooptées par l'Assemblée Générale , et qui
ont fait don à l’association de biens utiles à son objet et dont la
valeur est sans rapport avec le montant ordinaire des cotisations. Ils
disposent d’une voix consultative et sont invités à l’Assemblée
Générale.

### Article 7 - Perte de la qualité de membre

La qualité de membre se perd par :

– démission (via une lettre, un e-mail, ou verbalement et publiquement)

– non-renouvellement de la cotisation (l’exclusion prend effet selon les
modalités fixées par le règlement intérieur)

– décès

– radiation prononcée par le Conseil d’Administration selon les
modalités fixées par le règlement intérieur. Le Conseil d’Administration
peut aussi voter une suspension temporaire (moins de 1 an) qui aura tous
les effets d’une exclusion pendant le temps de la suspension.

### Article 8 - L’assemblée Générale Ordinaire

L’Assemblée Générale ordinaire est le lieu privilégié des débats
d’orientation et de stratégie du collectif : c’est l' organe décisionnel
de l’association.

Elle mandate les membres du Conseil d’Administration, et définit par le
biais de motions ouvertes à débat les décisions et orientations que le
Conseil d’Administration sera tenu d'appliquer.

C'est également l'Assemblée Générale qui valide les adhésions des
nouveaux membres actifs en leur présence, et, sur proposition du Conseil
d’Administration, se prononce sur les comptes de l’année écoulée et le
budget provisionnel, et fixe le montant annuel des cotisations. Elle
révise le Règlement Intérieur, sur proposition du Conseil
d’Administration.

Les décisions adoptées par l’Assemblée Générale ne peuvent être remises
en cause que lors d’une autre Assemblée Générale Ordinaire ou d’une
Assemblée Générale extraordinaire dûment convoquée à cet effet.

Elle se réunit au moins une fois par an, mais la fréquence de sa tenue,
les modalités de convocation, les conditions requises pour y participer
et y exercer son droit de vote, ainsi que les divers processus
décisionnels de l'Assemblée Générale sont définis par le Règlement
Intérieur.

### Article 9 : Le Conseil d’Administration

Le Conseil d’Administration - CA- a pour objet de mettre en œuvre les
décisions de l’Assemblée Générale. Il représente l’association dans tous
les actes de la vie civile et est investi de tous les pouvoirs à cet
effet. Il a notamment qualité pour ester en justice comme demandeur
et/ou défenseur au nom de l’association.

Ses modalités de fonctionnement sont précisées par le règlement
intérieur, notamment l'étendue de ses pouvoirs et compétences. Afin de
permettre une gouvernance partagée et le partage des pouvoirs et des
tâches par l’ensemble des membres, un renouvellement régulier des
membres du Conseil d’Administration sera recherché.

Il est composé de membre actifs adhérents élus par l’Assemblée Générale.

Il élit en son sein au moins un secrétaire et un trésorier. Le
secrétaire tient le compte-rendu des réunions du Conseil
d’Administration et d’Assemblée Générale. Le trésorier tient la
trésorerie et dispose de la signature des comptes bancaires.

Les modalités, la portée et les durées de ces mandats sont soit
prévisibles, et par conséquent spécifiés dans la définition du mandat,
soit relèvent de circonstances occasionnelles non-prévisibles, et feront
alors l'objet au cas par cas d'une convention qui devra être validée par
l'Assemblée Générale ou une Assemblée Générale Extraordinaire, notamment
en ce qui concerne la représentation de l'association en justice.

En cas de désaccord entre Conseil d’Administration et Assemblée
Générale, c'est cette dernière qui a pouvoir décisionnel final. Ce
pouvoir décisionnel peut aller jusque révoquer les mandats des membres
du Conseil d’Administration avant terme en Assemblée Générale
Extra-ordinaire en cas de conflit insoluble.

### Article 10 : Les ressources

Les ressources de l'association sont constituées par :

• les cotisations des membres,

• les dons de toute sorte, conformément à la réglementation en vigueur,

• les prix de prestations fournies par l'association,

• les subventions qui pourront lui être accordées

• toutes autres ressources ou subventions qui lui seraient accordées et
qui ne

seraient pas contraires aux lois en vigueur.

### Article 11 : Affiliation

L’association Esprit Fablab peut adhérer, sur proposition d'un membre
validée par l'Assemblée Générale selon les modalités prévues au
Règlement Intérieur, à toute association ou réseau..

### Article 12 : L’Assemblée Générale extra-ordinaire

Si besoin est, en cas de situation de crise ou de blocage constaté,
l’Assemblée Générale Extra-ordinaire est convoquée à la demande du
Conseil d’Administration ou d’un quart des membres actifs.

Les modalités de convocation et de délibération sont précisées dans le
cadre du règlement intérieur.

Pouvoirs de l'assemblée générale extraordinaire :

- remplacement de la totalité du CA

- modifier les statuts

- prononcer la dissolution de l’association

### Article 13 : Modification des statuts

Les présents statuts peuvent être modifiés suite à présentation d’une
motion à cet effet lors d’une Assemblée Générale Extra-ordinaire.

Les dispositions par lesquelles cette motion devra être rendue publique
sont énoncées au règlement intérieur.

### Article 14 : Dissolution

La décision de dissolution doit être débattue et décidée en Assemblée
Générale Extra-ordinaire convoquée dans les conditions suivantes :

– ordre du jour consacré à cet unique objet,

– quorum : au moins la moitié des adhérents à jour de leur cotisation,

– pouvoirs : au maximum deux pouvoirs par adhérent,

– règle de majorité : 2/3 des suffrages exprimés sont nécessaires pour
adopter la dissolution.

En cas de décision de dissolution, l’Assemblée Générale se prononcera
sur la dévolution des biens, et nommera un ou plusieurs liquidateurs
chargés de la liquidation des biens, de l’exécution des formalités
administratives et de l’information des partenaires.


