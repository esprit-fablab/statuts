# Dépôt des status de Esprit-Fablab

Ce dépôt contient contient les statuts de l’association Esprit-Fablab.

## Première proposition

En suivant ce lien, vous pourrez accéder à la première proposition des statuts d’Esprit-Fablab : [lien vers la proposition](https://framagit.org/esprit-fablab/statuts/blob/afccc1b9a5cd5ad08b5a3349eb4e500990204b5d/PropositionStatuts-6decembre-AR-AuPropre.md).
